# BlogAngular - Exercice Angular du site d'OpenClassrooms

Développement d'un simple blog de test en Angular.
Ce projet est un exercice du tutoriel d'Angular provenant du site d'OpenClassrooms.
(Lien du tutoriel : https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular)

Le projet a été généré automatiquement par [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Serveur de développement

La commande `ng serve` permet de lancer le serveur.<br>
L'adresse `http://localhost:4200/` d'accéder au projet une fois le serveur lancé. 
L'application se recharge automatiquement lors de changements dans les fichiers sources.

## Code scaffolding

La commande `ng generate component component-name` permet de générer un nouveau composant Angular.

## Build

La commande `ng build` permet de construire (builder) le projet et de générer les artéfacts dans le dossier `dist/`.<br>
A utiliser avec le paramètre `--prod` pour un build de production.

## Running unit tests & end-to-end tests

La commande `ng test` permet d'exécuter les tests unitaires via [Karma](https://karma-runner.github.io).
La commande `ng e2e` permet d'éxecuter les tests end-to-end via [Protractor](http://www.protractortest.org/).
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  articles = [
    {
      titre: 'Wurstsalat',
      contenu: 'Lorem Salu bissame ! Wie geht\'s les samis ? Hans apporte moi une Wurschtsalad avec un picon bitte, s\'il te plaît. Voss ? Une Carola et du Melfor ? Yo dû, espèce de Knäckes, ch\'ai dit un picon !',
      nbJAime: 2,
      dateCreation: new Date() 
    },
    {
      titre: 'Choucroute',
      contenu: 'Hopla vous savez que la mamsell Huguette, la miss Miss Dahlias du messti de Bischheim était au Christkindelsmärik en compagnie de Richard Schirmeck (celui qui a un blottkopf), le mari de Chulia Roberstau, qui lui trempait sa Nüdle dans sa Schneck ! Yo dû, Pfourtz ! Ch\'espère qu\'ils avaient du Kabinetpapier, Gal !',
      nbJAime: -1,
      dateCreation: new Date() 
    },
    {
      titre: 'Flammekueche',
      contenu: 'Yoo ch\'ai lu dans les DNA que le Racing a encore perdu contre Oberschaeffolsheim. Verdammi et moi ch\'avais donc parié deux knacks et une flammekueche. ',
      nbJAime: 0,
      dateCreation: new Date() 
    }
  ];
}
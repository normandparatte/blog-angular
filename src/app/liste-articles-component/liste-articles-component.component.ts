import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-liste-articles-component',
  templateUrl: './liste-articles-component.component.html',
  styleUrls: ['./liste-articles-component.component.scss']
})
export class ListeArticlesComponentComponent implements OnInit {
  @Input() listeArticles;  
  
  constructor() {
  }

  onAime(){
    // Problème lors de l'incrémentation (ne fonctionne pas)
    this.listeArticles.nbJAime++;    
  }

  onAimePas(){
    // Problème lors de la décrémentation (ne fonctionne pas)
    this.listeArticles.nbJAime--;
  }

  ngOnInit() {
  }
}